package servlets;

import repositories.IRepoCatalog;
import repositories.impl.RepoCatalogProvider;
import domain.Role;

public class RoleServletLogic {

	IRepoCatalog catalog;
	
	public RoleServletLogic() {
		catalog = RepoCatalogProvider.catalog();
	}
	
	public void addNewRole(HttpServletRequest request)
	{
		Role a = new Role();
		a.setName(request.getParameter("name"));
		catalog.getRoles().save(a);
		catalog.commit();
	}
	
	public String showRoleInhtmlForm()
	{
		String html = "<ol>";
		for(Role u: catalog.getRoles().getAll())
		{
			html+="<li>"
					+ u.getName()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}