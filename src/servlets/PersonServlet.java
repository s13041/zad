package servlets;

import java.io.IOException;

@WebServlet("/ShowPerson")
public class PersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	PersonServletLogic logic = new PersonServletLogic();
    public PersonServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print(logic.showPersonInhtmlForm());
		
	}
}