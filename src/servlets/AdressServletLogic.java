package servlets;

import javax.servlet.http.HttpServletRequest;

import domain.Address;
import domain.User;
import repositories.IRepoCatalog;
import repositories.impl.RepoCatalogProvider;

public class AdressServletLogic {

	IRepoCatalog catalog;
	
	public AdressServletLogic() {
		catalog = RepoCatalogProvider.catalog();
	}
	
	public void addNewAdress(HttpServletRequest request)
	{
		Address a = new Address();
		a.setCity(request.getParameter("city"));
		a.setCountry(request.getParameter("country"));
		catalog.getAddress().save(a);
		catalog.commit();
	}
	
	public String showAddressInhtmlForm()
	{
		String html = "<ol>";
		for(Address u: catalog.getAddress().getAll())
		{
			html+="<li>"
					+ u.getStreet()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}