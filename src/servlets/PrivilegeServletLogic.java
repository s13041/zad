package servlets;

import javax.servlet.http.HttpServletRequest;

import domain.Privilege;
import domain.User;
import repositories.IRepoCatalog;
import repositories.impl.RepoCatalogProvider;

public class PrivilegeServletLogic {

	IRepoCatalog catalog;
	
	public PrivilegeServletLogic() {
		catalog = RepoCatalogProvider.catalog();
	}
	
	public void addNewPrivilege(HttpServletRequest request)
	{
		Privilege p = new Privilege();
		p.setName(request.getParameter("name"));
		
		catalog.getPriviliges().save(p);
		catalog.commit();
	}
	
	public String showPrivilegeInhtmlForm()
	{
		String html = "<ol>";
		for(Privilege u: catalog.getPriviliges().getAll())
		{
			html+="<li>"
					+ u.getName()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}